package com.example.urubu.urubu.webservice;

import java.util.List;

/**
 * Created by Ruan on 19/11/2015.
 */
public interface MessageInterface {

    void populateMsgsOnScreen(List<Message> messages);
    void redirectToHome();
}
