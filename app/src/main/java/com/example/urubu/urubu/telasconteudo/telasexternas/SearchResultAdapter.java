package com.example.urubu.urubu.telasconteudo.telasexternas;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.example.urubu.urubu.LoadImage;
import com.example.urubu.urubu.R;
import com.example.urubu.urubu.webservice.Settings;
import com.example.urubu.urubu.webservice.User;

import java.util.List;

/**
 * Created by Ruan on 28/10/2015.
 */
public class SearchResultAdapter extends BaseAdapter{

    List<User> listUsers;

    public SearchResultAdapter(List<User> listUsers){
        this.listUsers = listUsers;
    }


    @Override
    public int getCount() {
        return listUsers.size();
    }

    @Override
    public Object getItem(int i) {
        return listUsers.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null){

            LayoutInflater inflater = (LayoutInflater) viewGroup.getContext().getSystemService(viewGroup.getContext().LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_item_search,null);

            User usr = listUsers.get(i);
            TextView username = (TextView) view.findViewById(R.id.txt_username);
            TextView email = (TextView) view.findViewById(R.id.txt_email);
            ImageView usrPic = (ImageView) view.findViewById(R.id.usr_picture);

            //Set values into the screen components
            username.setText(usr.get_name() + " " + usr.get_lastName());
            email.setText(usr.get_email() + " " + usr.get_email());

            //Verifica se existe imagem no post, faz o download e apresenta na timeline
            if(usr.get_avatar().length() > 0){
                String sImgAva = Settings.URL_uploads + usr.get_avatar();
                ImageLoader avaLoad = LoadImage.getInstance(view.getContext()).getImageLoader();
                avaLoad.get(sImgAva, avaLoad.getImageListener(usrPic,
                                R.drawable.ic_account_circle_black_48dp,
                                R.drawable.ic_account_circle_black_48dp),
                        600,
                        600
                );
            }

        }

        return view;
    }
}
