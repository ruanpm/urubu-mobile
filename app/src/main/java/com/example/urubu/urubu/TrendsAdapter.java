package com.example.urubu.urubu;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.urubu.urubu.webservice.Trends;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by Norton on 22/11/2015.
 */
public class TrendsAdapter extends BaseAdapter {

    private List<Trends> trends;

    public TrendsAdapter(List<Trends> l){
        this.trends = l;
    }

    @Override
    public int getCount() {
        return this.trends.size();
    }

    @Override
    public Object getItem(int i) {
        return this.trends.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        try{
            Trends t = trends.get(i);

            LayoutInflater inflater = (LayoutInflater) viewGroup.getContext().getSystemService(viewGroup.getContext().LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_trends,null);

            TextView txtTrend = (TextView)view.findViewById(R.id.txtTrend);
            txtTrend.setText(t.getTrend());

            return view;
        } catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }
}
