package com.example.urubu.urubu.telasconteudo.telasexternas;

import java.io.Serializable;

/**
 * Created by Norton on 21/11/2015.
 */
public class FollowersData implements Serializable {

    private String name;
    private String lastName;
    private String userName;
    private String about;
    private String avatar;

    public FollowersData() {
        this.name = "";
        this.lastName = "";
        this.userName = "";
        this.about = "";
        this.avatar = "";
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
