package com.example.urubu.urubu.webservice;

import android.content.Context;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.urubu.urubu.R;
import com.example.urubu.urubu.database.Login;
import com.example.urubu.urubu.database.LoginDAO;
import com.example.urubu.urubu.telasconteudo.telasexternas.FollowersData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Norton on 08/11/2015.
 */
public class NewFollowsWebDAO {

    private Context context;
    private MainTela tela;

    public NewFollowsWebDAO(Context context, MainTela tela){
        this.context = context;
        this.tela = tela;
    }

    public void getNewFriendInvites(){
        try{
            //Obtem usuario logado
            LoginDAO oLoginDAO = new LoginDAO(context);
            final Login oLogin = oLoginDAO.getLogin();

            //Obtem url
            String sUrl = Settings.URL + "users/news";

            //Prepara JSON
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("userid", oLogin.getUserID());
            jsonParams.put("token", oLogin.getToken());

            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                    (Request.Method.POST, sUrl, jsonParams, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response){
                            try {
                                String sReturn = response.getString("return");
                                if(!sReturn.equalsIgnoreCase("NOTFOUND") && !sReturn.equalsIgnoreCase("ERROR") && !sReturn.equalsIgnoreCase("NOT_LOGGED") ){
                                    JSONArray oJsArray = response.getJSONArray("return");
                                    if(oJsArray.length() > 0){
                                        ArrayList<FollowersData> fol = new ArrayList<FollowersData>();

                                        for(int i=0; i<oJsArray.length(); i++){
                                            JSONObject ojs = oJsArray.getJSONObject(i);
                                            FollowersData oFol = new FollowersData();
                                            oFol.setAbout(ojs.getString("about"));
                                            oFol.setAvatar(ojs.getString("avatar"));
                                            oFol.setName(ojs.getString("name"));
                                            oFol.setLastName(ojs.getString("lastname"));
                                            oFol.setUserName(ojs.getString("username"));
                                            fol.add(oFol);
                                        }

                                        tela.showNewFriendsNotify(fol);
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            try {
                                throw new Exception(error.toString());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

            RequestQueue queue = Volley.newRequestQueue(context);
            queue.add(jsObjRequest);

        } catch(Exception ex){
            ex.printStackTrace();
        }
    }

}
