package com.example.urubu.urubu.telasconteudo.telasinternas;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;

import com.example.urubu.urubu.TrendsAdapter;
import com.example.urubu.urubu.webservice.Message;
import com.example.urubu.urubu.webservice.TimelineWebDAO;
import com.example.urubu.urubu.webservice.Trends;
import com.example.urubu.urubu.webservice.TrendsDAO;
import com.example.urubu.urubu.webservice.TrendsTela;

import java.util.List;

/**
 * Created by Norton on 22/11/2015.
 */
public class TrendsFragment extends ListFragment implements TrendsTela {

    OnMessageSelectedListener mCallback;
    List<Trends> lTrends = null;

    public interface OnMessageSelectedListener {
        /** Called by MensagensFragment when a list item is selected */
        public void onTrendsSelected(Trends PoTrend);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Obtem dados do webservice
        TrendsDAO oTrendsDao = new TrendsDAO(getContext(), this);
        oTrendsDao.obtemTrends();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallback = (OnMessageSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        // Notify the parent activity of selected item
        Trends oTrend = lTrends.get(position);
        mCallback.onTrendsSelected(oTrend);
    }

    @Override
    public void listaTrends(List<Trends> PoListTrends) {
        lTrends = PoListTrends;
        setListAdapter(new TrendsAdapter(lTrends));
    }
}
