package com.example.urubu.urubu.webservice;

import android.content.Context;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Norton on 22/11/2015.
 */
public class TrendsDAO {

    private Context context;
    private TrendsTela tela;

    public TrendsDAO(Context context, TrendsTela tela) {
        this.context = context;
        this.tela = tela;
    }

    public void obtemTrends(){
        try{

            //URL trends
            String sUrl = Settings.URL + "trends";

            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                    (sUrl, null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                String sReturn = response.getString("return");
                                List<Trends> lTrends = new ArrayList<Trends>();

                                if(!sReturn.equalsIgnoreCase("ERROR")){
                                    JSONArray oJsArray = response.getJSONArray("return");
                                    for(int i=0;i<oJsArray.length(); i++){
                                        JSONObject oObject = oJsArray.getJSONObject(i);

                                        Trends oTrend = new Trends();
                                        oTrend.setPos(oObject.getInt("pos"));
                                        oTrend.setTrend(oObject.getString("trend"));

                                        lTrends.add(oTrend);
                                    }
                                } else {
                                    throw new Exception("Erro ao obter os trends");
                                }

                                tela.listaTrends(lTrends);
                            } catch (JSONException e) {
                                try {
                                    throw new Exception(e.getMessage());
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                }
                            } catch (Exception e) {
                                try {
                                    throw new Exception(e.getMessage());
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                }
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            try {
                                throw new Exception(error.toString());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

            RequestQueue queue = Volley.newRequestQueue(context);
            queue.add(jsObjRequest);

        } catch (Exception ex){
            Log.d("URUBU", ex.getMessage());
            List<Trends> lTrends = new ArrayList<Trends>();
            tela.listaTrends(lTrends);
        }
    }
}
