package com.example.urubu.urubu.telasconteudo.telasexternas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.example.urubu.urubu.R;

import java.util.ArrayList;
import java.util.List;

public class NewFollowers extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_followers);

        Bundle oBundle = getIntent().getExtras();
        List<FollowersData> fol = (ArrayList<FollowersData>)oBundle.getSerializable("FOLLOWERS");
        ListView lview = (ListView)findViewById(R.id.listFollowers);

        NewFollowersAdapter oAdap = new NewFollowersAdapter(getApplicationContext(), fol);
        lview.setAdapter(oAdap);
        lview.setEmptyView(findViewById(R.id.txtListEmpty));
    }


}
