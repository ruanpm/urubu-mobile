package com.example.urubu.urubu.webservice;

/**
 * Created by Norton on 22/11/2015.
 */
public class Trends {

    private int pos;
    private String trend;

    public Trends() {
        this.pos = 0;
        this.trend = "";
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public String getTrend() {
        return trend;
    }

    public void setTrend(String trend) {
        this.trend = trend;
    }
}
