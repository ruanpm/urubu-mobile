package com.example.urubu.urubu.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Norton on 04/10/2015.
 */
public class LoginDbHelper extends SQLiteOpenHelper {
    private static final String TEXT_TYPE = " TEXT";
    private static final String REAL_TYPE = " REAL";
    private static final String NUMERIC_TYPE = " NUMERIC";
    private static final String COMMA_SEP = ",";

    private static final String SQL_CREATE_TABLE_LOGIN =
        "CREATE TABLE " + LoginContract.Login.TABLE_NAME + " (" +
                LoginContract.Login.USERNAME + TEXT_TYPE + COMMA_SEP +
                LoginContract.Login.TOKEN + TEXT_TYPE + COMMA_SEP +
                LoginContract.Login.TOKENVAL + TEXT_TYPE + COMMA_SEP +
                LoginContract.Login.USERID + TEXT_TYPE +
                " )";

    private static final String SQL_DELETE_TABLE_LOGIN =
            "DROP TABLE IF EXISTS " + LoginContract.Login.TABLE_NAME;

    // Se você modificar o schema do banco, você deve incrementar a versão do software.
    public static final int DATABASE_VERSION = 5;
    public static final String DATABASE_NAME = "LOGIN.db";

    public LoginDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_LOGIN);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_TABLE_LOGIN);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
