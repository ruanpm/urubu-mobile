package com.example.urubu.urubu.telasconteudo.telasexternas;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.urubu.urubu.GPSTracker;
import com.example.urubu.urubu.MainActivity;
import com.example.urubu.urubu.R;
import com.example.urubu.urubu.webservice.Message;
import com.example.urubu.urubu.webservice.MessageDAO;
import com.example.urubu.urubu.webservice.MessageInterface;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class NewMessage extends AppCompatActivity implements MessageInterface{

    ProgressDialog lDialog;
    static final int REQUEST_TAKE_PHOTO = 1;
    static final int REQUEST_EDIT_PHOTO = 2;
    static final int REQUEST_PHOTO_GALLERY = 3;
    private File fPicture;
    private Location location;
    public static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_message);

        this.context = this;

        lDialog = ProgressDialog.show(NewMessage.this, "", getResources().getString(R.string.loading), true);

        //DEFINE EVENTO DE CLIQUE NO ICONE DE IMAGEM
        final ImageView picture = (ImageView) findViewById(R.id.add_picture);
        registerForContextMenu(picture);
        picture.setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(View v) {
                //To register the button with context menu.
                registerForContextMenu(picture);
                openContextMenu(picture);
            }
        });

        lDialog.hide();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.add(0, 1, 0, R.string.take_photo);
        menu.add(0, 2, 0, R.string.choose_from_album);

        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1: {
                takePictureIntent();
            }
            break;
            case 2: {
                getPictureFromGallery();
            }
            break;
        }
        return super.onContextItemSelected(item);
    }


    public void takePictureIntent() {
        try{
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            // Ensure that there's a camera activity to handle the intent
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                // Create the File where the photo should go
                fPicture = null;
                try {
                    fPicture = createImageFile();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                // Continue only if the File was successfully created
                if (fPicture != null) {
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fPicture));
                    startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                }
            }
        } catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public void getPictureFromGallery(){
        try{
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, REQUEST_PHOTO_GALLERY);
        } catch(Exception ex){
            ex.printStackTrace();
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";

        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES + "UrubuApp/");

        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }

        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        //mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_TAKE_PHOTO: {

                    new AlertDialog.Builder(this)
                            .setMessage(R.string.txt_ask_edit_pic)
                            .setCancelable(false)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent editIntent = new Intent(Intent.ACTION_EDIT);
                                    editIntent.setDataAndType(Uri.fromFile(fPicture), "image/*");
                                    editIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                    startActivityForResult(Intent.createChooser(editIntent, null), REQUEST_EDIT_PHOTO);
                                }
                            })
                            .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    addImage(data);
                                }
                            })
                            .show();
                }
                break;
                case REQUEST_EDIT_PHOTO: {
                    addImage(data);
                }
                break;
                case REQUEST_PHOTO_GALLERY: {
                    new AlertDialog.Builder(this)
                            .setMessage(R.string.txt_ask_edit_pic)
                            .setCancelable(false)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent editIntent = new Intent(Intent.ACTION_EDIT);
                                    editIntent.setDataAndType(data.getData(), "image/*");
                                    editIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                    startActivityForResult(Intent.createChooser(editIntent, null), REQUEST_EDIT_PHOTO);
                                }
                            })
                            .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    addImage(data);
                                }
                            })
                            .show();
                }
                break;
            }
        } else if (resultCode == RESULT_CANCELED) {
            // Use the Builder class for convenient dialog construction
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.camera_cancelled)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) { }
                    });
            // Create the AlertDialog object and return it
            AlertDialog oDialog = builder.create();
            oDialog.show();
        } else {
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.camera_fail)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) { }
                    });
            // Create the AlertDialog object and return it
            AlertDialog oDialog = builder.create();
            oDialog.show();
        }
    }

    public void addImage(Intent data){

        Uri selectedImageURI = null;
        if(data == null){
            selectedImageURI = Uri.fromFile(fPicture);
        }
        else {
            selectedImageURI = data.getData();
        }

        fPicture = new File(getRealPathFromURI(selectedImageURI));

        //tenta achar o layout das mini pictures se nao achar entao cria
        LinearLayout linearLay = (LinearLayout) findViewById(R.id.layout_mini_pictures);
        if (linearLay == null) {

            //Cria LinearLayout que contera as miniaturas das imgs de upload
            linearLay = new LinearLayout(NewMessage.context);
            linearLay.setId(R.id.layout_mini_pictures);
            LinearLayout.LayoutParams LLParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 200);
            LLParams.setMargins(0, 10, 0, 0);
            linearLay.setOrientation(LinearLayout.HORIZONTAL);
            linearLay.setLayoutParams(LLParams);

            //miniatura da img de upload
            ImageView miniPicture = new ImageView(NewMessage.context);
            LinearLayout.LayoutParams LLParamMiniPic = new LinearLayout.LayoutParams(220, 280);
            LLParamMiniPic.setMargins(30, -50, 0, 100);
            miniPicture.setLayoutParams(LLParamMiniPic);
            miniPicture.setImageURI(Uri.fromFile(fPicture)); //seta imagem
            miniPicture.setScaleType(ImageView.ScaleType.FIT_CENTER);

            //Add mini picture dentro do novo layout
            linearLay.addView(miniPicture);

            //Adiciona o novo layout a um layout root ja presenta na tela
            LinearLayout linearLayRoot = (LinearLayout) findViewById(R.id.message_box);
            linearLayRoot.addView(linearLay, 1); //adiciona na posicai acima da barra de menu
        } else {

            //miniatura da img de upload
            ImageView miniPicture = new ImageView(NewMessage.context);
            LinearLayout.LayoutParams LLParamMiniPic = new LinearLayout.LayoutParams(220, 280);
            LLParamMiniPic.setMargins(30, -50, 0, 100);
            miniPicture.setLayoutParams(LLParamMiniPic);
            miniPicture.setImageURI(data.getData()); //seta imagem
            miniPicture.setScaleType(ImageView.ScaleType.FIT_CENTER);

            //Add mini picture dentro do novo layout
            linearLay.addView(miniPicture);
        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_message, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }

    public String getBase64(String PsFilePath){
        Bitmap bm = BitmapFactory.decodeFile(PsFilePath);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 10, baos);

        byte[] b = baos.toByteArray();
        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encodedImage;
    }

    public void sendMessage(View view){

        lDialog = ProgressDialog.show(NewMessage.this, "", getResources().getString(R.string.loading), true);

        String imageName = "tmp.jpeg";
        String image = fPicture == null ? null : getBase64(fPicture.getPath());
        String text = ((EditText) findViewById(R.id.txtNewMessage)).getText().toString();

        String lat = null;
        String lng = null;

        if(this.location != null){
            lat = String.valueOf(this.location.getLatitude());
            lng = String.valueOf(this.location.getLongitude());
        }

        MessageDAO msgDAO = new MessageDAO(this);
        msgDAO.sendNewMessage(imageName, image, text, lat, lng, this);
    }

    public void getLocation(View view) {
        lDialog = ProgressDialog.show(NewMessage.this, "", getResources().getString(R.string.loading), true);

        GPSTracker gps = new GPSTracker(this);
        if (gps.canGetLocation()) { // gps enabled} // return boolean true/false
            location  = gps.getLocation();
            //Changes gps icon to look like activated
            ImageView btnGPS = (ImageView) findViewById(R.id.add_location);
            btnGPS.setImageResource(R.drawable.ic_location_active_on_blue);

            String address = gps.getAddress(location.getLatitude(), location.getLongitude());
            TextView txt_address = (TextView) findViewById(R.id.txt_address_gps);
            txt_address.setText(address);
        }

        lDialog.hide();
    }

    public void redirectToHome(){
        lDialog.hide();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void populateMsgsOnScreen(List<Message> messages) {
        //NOT USED
    }
}
