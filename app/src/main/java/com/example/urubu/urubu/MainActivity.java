package com.example.urubu.urubu;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.urubu.urubu.database.Login;
import com.example.urubu.urubu.database.LoginDAO;
import com.example.urubu.urubu.telasconteudo.telasexternas.FollowersData;
import com.example.urubu.urubu.telasconteudo.telasexternas.NewFollowers;
import com.example.urubu.urubu.telasconteudo.telasexternas.NewMessage;
import com.example.urubu.urubu.telasconteudo.telasinternas.TimelineFragment;
import com.example.urubu.urubu.telasconteudo.telasinternas.TrendsFragment;
import com.example.urubu.urubu.webservice.MainTela;
import com.example.urubu.urubu.webservice.Message;
import com.example.urubu.urubu.webservice.NewFollowsWebDAO;
import com.example.urubu.urubu.webservice.Trends;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity
        extends AppCompatActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks,
            TimelineFragment.OnMessageSelectedListener,
            TrendsFragment.OnMessageSelectedListener,
            MainTela {

    private NavigationDrawerFragment mNavigationDrawerFragment;
    private CharSequence mTitle;
    private static final String TAG_FRAGMENT = "TIMELINE";
    private static final String TAG_TRENDS = "TRENDS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Instancia fragmento
        Fragment fragment = new TimelineFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .add(R.id.container, fragment, TAG_FRAGMENT)
                .commit();

        //check if the user is logged
        chkUserIsLogged();

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        //Verifica solicitações de novas amizades
        NewFollowsWebDAO oNFWD = new NewFollowsWebDAO(getApplicationContext(), this);
        oNFWD.getNewFriendInvites();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        Fragment fragment = null;
        FragmentManager fragmentManager = getSupportFragmentManager();

        switch (position) {
            case 0:
                mTitle = getString(R.string.title_home);

                //Remove fragment atual
                Fragment frg = getSupportFragmentManager().findFragmentByTag(TAG_FRAGMENT);
                if(frg != null)
                    getSupportFragmentManager().beginTransaction().remove(frg).commit();

                //Adiciona nova fragment
                fragment = new TimelineFragment();
                fragmentManager.beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.container, fragment)
                        .commit();
                break;
            case 1:
                mTitle = getString(R.string.title_trends);
                Fragment frgTrends = getSupportFragmentManager().findFragmentByTag(TAG_TRENDS);
                if(frgTrends != null)
                    getSupportFragmentManager().beginTransaction().remove(frgTrends).commit();

                //Adiciona nova fragment
                fragment = new TrendsFragment();
                fragmentManager.beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.container, fragment)
                        .commit();
                break;
            case 2:
                mTitle = getString(R.string.title_config);
                startActivity(new Intent(this, SettingsActivity.class));
                break;
            case 3:
                LoginDAO loginDAO = new LoginDAO(this);
                //Delete the person register from local database so the log out is done
                loginDAO.deletePessoa(loginDAO.getLogin());
                startActivity(new Intent(this, LoginActivity.class)); //Redirect to login screen
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            //restoreActionBar();

            // Associate searchable configuration with the SearchView
            SearchManager searchManager =  (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("selected_navigation_drawer_position", 0);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        Fragment fragment = null;
        FragmentManager fragmentManager = getSupportFragmentManager();

        //Remove fragment atual
        Fragment frg = getSupportFragmentManager().findFragmentByTag(TAG_FRAGMENT);
        if(frg != null)
            getSupportFragmentManager().beginTransaction().remove(frg).commit();

        //Adiciona nova fragment
        fragment = new TimelineFragment();
        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .commit();

        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.newmessage) {
            Intent iNewMessage = new Intent(this, NewMessage.class);
            startActivity(iNewMessage);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onArticleSelected(Message PoMessage) {
        Intent iMDActivity = new Intent(this, MessageDetailActivity.class);
        Bundle bArgs = new Bundle();
        bArgs.putSerializable("MSG", PoMessage);
        iMDActivity.putExtras(bArgs);
        startActivity(iMDActivity);
    }

    @Override
    public void showNewFriendsNotify(ArrayList<FollowersData> lData) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.urubu_menu)
                        .setContentTitle(getResources().getString(R.string.notify_title))
                        .setContentText(getResources().getString(R.string.notify_subtitle));

        //Configurando Intent para abrir tela quando clicar na notificacao
        Intent resultIntent = new Intent(this, NewFollowers.class);

        Bundle bundle = new Bundle();
        bundle.putSerializable("FOLLOWERS", lData);
        resultIntent.putExtras(bundle);

        // Because clicking the notification opens a new ("special") activity, there's
        // no need to create an artificial back stack.
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        //configurando a acao do clique na notificacao
        mBuilder.setContentIntent(resultPendingIntent);
        mBuilder.setAutoCancel(true);

        // Sets an ID for the notification
        int mNotificationId = 001;

        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

    //Check if the user is logged, if not then call the log in screen
    public void chkUserIsLogged(){
        try {
            LoginDAO oLoginDao = new LoginDAO(this.getApplicationContext());
            if (oLoginDao.databaseExists()) {
                Login oLogin = oLoginDao.getLogin();
                if (oLogin != null) {
                    validateToken(oLogin);
                } else {
                    startActivity(new Intent(this, LoginActivity.class));
                }
            } else {
                startActivity(new Intent(this, LoginActivity.class));
            }
        }
        catch(ParseException e){
            e.printStackTrace();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public void validateToken(Login PoLogin) throws ParseException {
        SimpleDateFormat oDtFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            Date dtTokenVal = oDtFormat.parse(PoLogin.getTokenVal());
            int res = dtTokenVal.compareTo(new Date());
            if (res == -1) {
                Toast.makeText(getApplicationContext(), "Seu login expirou!", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(this, LoginActivity.class));
            }
        } catch (ParseException e) {
            throw e;
        }
    }


    @Override
    public void onTrendsSelected(Trends PoTrend) {
        SearchView searchView = (SearchView) findViewById(R.id.search);

        String sQuery = "";
        if(PoTrend.getTrend().startsWith("#")){
            sQuery = PoTrend.getTrend();
        } else {
            sQuery = "#" + PoTrend.getTrend();
        }
        searchView.setQuery(sQuery, true);
    }
}
