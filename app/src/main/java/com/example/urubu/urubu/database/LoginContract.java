package com.example.urubu.urubu.database;

import android.provider.BaseColumns;

/**
 * Created by Norton on 04/10/2015.
 */
public final class LoginContract {

    public LoginContract(){}

    public static abstract class Login implements BaseColumns{
        public final static String TABLE_NAME = "LOGIN";
        public final static String USERNAME = "username";
        public final static String USERID = "userid";
        public final static String TOKEN = "token";
        public final static String TOKENVAL = "tokenval";
    }
}
