package com.example.urubu.urubu.webservice;

import java.io.Serializable;

/**
 * Created by Norton on 31/10/2015.
 */
public class Location implements Serializable{
    private String lat;
    private String lng;

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public Location() {
        this.lat = "";
        this.lng = "";
    }
}
