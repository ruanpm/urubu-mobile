package com.example.urubu.urubu.telasconteudo.telasexternas;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.example.urubu.urubu.LoadImage;
import com.example.urubu.urubu.R;
import com.example.urubu.urubu.webservice.Message;
import com.example.urubu.urubu.webservice.Settings;

import java.util.List;

/**
 * Created by Norton on 21/11/2015.
 */
public class NewFollowersAdapter extends BaseAdapter {

    List<FollowersData> lUsers;
    Context context;

    public NewFollowersAdapter(Context context, List<FollowersData> listFollowers) {
        this.lUsers = listFollowers;
        this.context = context;
    }

    @Override
    public int getCount() {
        return this.lUsers.size();
    }

    @Override
    public Object getItem(int i) {
        return this.lUsers.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(viewGroup.getContext().LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_followers, null);

            //Objeto seguidor
            FollowersData fol = lUsers.get(i);

            if (fol.getAvatar().length() > 0) {
                ImageView imgAvatar = (ImageView) view.findViewById(R.id.imgUserF);
                String sImgAva = Settings.URL_uploads + fol.getAvatar();
                ImageLoader avaLoad = LoadImage.getInstance(view.getContext()).getImageLoader();
                avaLoad.get(sImgAva, avaLoad.getImageListener(imgAvatar,
                                R.drawable.ic_account_circle_black_48dp,
                                R.drawable.ic_account_circle_black_48dp),
                        600,
                        600
                );
            }

            TextView txtName = (TextView)view.findViewById(R.id.txtNameF);
            txtName.setText(fol.getName() + " " + fol.getLastName());

            TextView txtUserName = (TextView)view.findViewById(R.id.txtUserNameF);
            txtUserName.setText(fol.getUserName());

            TextView txtAbout = (TextView)view.findViewById(R.id.txtUserAboutF);
            txtUserName.setText(fol.getAbout());

        }

        return view;
    }
}
