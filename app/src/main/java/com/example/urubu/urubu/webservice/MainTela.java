package com.example.urubu.urubu.webservice;

import com.example.urubu.urubu.telasconteudo.telasexternas.FollowersData;

import java.util.ArrayList;

/**
 * Created by Norton on 08/11/2015.
 */
public interface MainTela {

    public void showNewFriendsNotify(ArrayList<FollowersData> lData);

}
