package com.example.urubu.urubu;

import android.app.ProgressDialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.urubu.urubu.webservice.CreateUserWebDAO;
import com.example.urubu.urubu.webservice.CriarUsuarioTela;

public class CreateUserActivity extends ActionBarActivity implements CriarUsuarioTela {

    ProgressDialog lDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);
    }

    public void validaLogin(View v){
        EditText edtName = (EditText)findViewById(R.id.edtName);
        EditText edtLastName = (EditText)findViewById(R.id.edtLastName);
        EditText edtUserName = (EditText)findViewById(R.id.edtUserName);
        EditText edtPassword = (EditText)findViewById(R.id.edtPassword);
        EditText edtEmail = (EditText)findViewById(R.id.edtEmail);

        String sName = edtName.getText().toString();
        String sLastName = edtLastName.getText().toString();
        String sUserName = edtUserName.getText().toString();
        String sPassword = edtPassword.getText().toString();
        String sEmail = edtEmail.getText().toString();

        boolean bValid = true;

        if(sName.length() == 0 && bValid){
            Toast.makeText(getApplicationContext(), R.string.name_error, Toast.LENGTH_SHORT).show();
            bValid = false;
        }

        if(sLastName.length() == 0 && bValid){
            Toast.makeText(getApplicationContext(), R.string.lastname_error, Toast.LENGTH_SHORT).show();
            bValid = false;
        }

        if(sUserName.length() == 0 && bValid){
            Toast.makeText(getApplicationContext(), R.string.username_error, Toast.LENGTH_SHORT).show();
            bValid = false;
        }

        if(sPassword.length() == 0 && bValid){
            Toast.makeText(getApplicationContext(), R.string.pass_empty, Toast.LENGTH_SHORT).show();
            bValid = false;
        }

        if(sEmail.length() == 0 && bValid){
            Toast.makeText(getApplicationContext(), R.string.email_blank, Toast.LENGTH_SHORT).show();
            bValid = false;
        }

        if(!validaEmail(sEmail) && bValid){
            Toast.makeText(getApplicationContext(), R.string.email_error, Toast.LENGTH_SHORT).show();
            bValid = false;
        }

        if(bValid){
            lDialog = ProgressDialog.show(CreateUserActivity.this, "", getResources().getString(R.string.loading), true);
            CreateUserWebDAO oCreateDAO = new CreateUserWebDAO(getApplicationContext(), this);
            oCreateDAO.CreateUser(sName, sLastName, sUserName, sPassword, sEmail);
        }
    }

    public boolean validaEmail(String PsEmail){
        if (Patterns.EMAIL_ADDRESS.matcher(PsEmail).matches())
            return true;
        else
            return false;
    }

    @Override
    public void createReturn(String PsReturn) {
        try {
            lDialog.hide();
            String sReturn = PsReturn;

            if(sReturn.equalsIgnoreCase("USER_CREATED")){
                String sCreateUserMsg = this.getResources().getString(R.string.createuserok);
                Toast.makeText(this, sCreateUserMsg, Toast.LENGTH_SHORT).show();
            } else {
                String sError = "";
                if(sReturn.equalsIgnoreCase("EMAIL_FAIL")){
                    sError = this.getResources().getString(R.string.email_fail);
                    Toast.makeText(this, sError, Toast.LENGTH_SHORT).show();
                }

                if(sReturn.equalsIgnoreCase("UNAME_FAIL")){
                    sError = this.getResources().getString(R.string.uname_fail);
                    Toast.makeText(this, sError, Toast.LENGTH_SHORT).show();
                }

                if(sReturn.equalsIgnoreCase("INVALID_PASSWORD")){
                    sError = this.getResources().getString(R.string.invalid_password);
                    Toast.makeText(this, sError, Toast.LENGTH_SHORT).show();
                }

                if(sReturn.equalsIgnoreCase("USER_NOT_CREATED")){
                    sError = this.getResources().getString(R.string.user_not_created);
                    Toast.makeText(this, sError, Toast.LENGTH_SHORT).show();
                }

                if(sReturn.equalsIgnoreCase("ERROR")){
                    sError = this.getResources().getString(R.string.error);
                    Toast.makeText(this, sError, Toast.LENGTH_SHORT).show();
                }
            }

            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
