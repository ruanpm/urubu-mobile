package com.example.urubu.urubu.webservice;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.urubu.urubu.CreateUserActivity;
import com.example.urubu.urubu.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Norton on 02/10/2015.
 */
public class CreateUserWebDAO {

    private Context context;
    private CriarUsuarioTela tela;

    public CreateUserWebDAO(Context context, CriarUsuarioTela tela){
        this.context = context;
        this.tela = tela;
    }

    public void CreateUser(String PsName, String PsLastName, String PsUserName, String PsPassword, String PsEmail){
        try{
            String sUrl = Settings.URL + "users";

            JSONObject jsonParams = new JSONObject();
            jsonParams.put("name", PsName);
            jsonParams.put("lastname", PsLastName);
            jsonParams.put("username", PsUserName);
            jsonParams.put("password", PsPassword);
            jsonParams.put("about", "");
            jsonParams.put("email", PsEmail);

            JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, sUrl, jsonParams, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response){
                        try {
                            String sReturn = response.getString("return");
                            tela.createReturn(sReturn);
                        } catch (JSONException e) {
                            try {
                                throw new Exception(e.toString());
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            throw new Exception(error.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

            RequestQueue queue = Volley.newRequestQueue(context);
            queue.add(jsObjRequest);
        }catch (Exception ex){
            Log.e("UsersWebDAO", ex.getMessage());
            tela.createReturn("ERROR");
        }
    }
}
